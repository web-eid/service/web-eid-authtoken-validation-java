package org.webeid.security.exceptions;

/**
 * Thrown when the nonce has expired.
 */
public class NonceExpiredException extends TokenValidationException {
    public NonceExpiredException() {
        super("Nonce has expired");
    }
}
