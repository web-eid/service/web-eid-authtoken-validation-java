package org.webeid.security.exceptions;

/**
 * Thrown when the nonce was not found in the cache.
 */
public class NonceNotFoundException extends TokenValidationException {
    public NonceNotFoundException() {
        super("Nonce was not found in cache");
    }
}
