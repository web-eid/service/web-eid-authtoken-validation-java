package org.webeid.security.exceptions;

/**
 * Thrown when the origin URL from the token {@code aud} field does not match the configured origin,
 * i.e. the URL that the site is running on.
 */
public class OriginMismatchException extends TokenValidationException {

    private static final String MESSAGE = "Origin from the token does not match the configured origin";

    public OriginMismatchException() {
        super(MESSAGE);
    }

    public OriginMismatchException(Throwable cause) {
        super(MESSAGE + ":", cause);
    }
}
