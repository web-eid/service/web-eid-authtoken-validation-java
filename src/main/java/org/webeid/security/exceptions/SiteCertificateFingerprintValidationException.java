package org.webeid.security.exceptions;

/**
 * Thrown when the site HTTPS certificate fingerprint from the token {@code aud} field does not match
 * the configured site certificate fingerprint that the site is using.
 */
public class SiteCertificateFingerprintValidationException extends TokenValidationException {
    public SiteCertificateFingerprintValidationException() {
        super("Server certificate fingerprint does not match the configured fingerprint");
    }
}
