package org.webeid.security.exceptions;

/**
 * Thrown when the authentication token has expired.
 */
public class TokenExpiredException extends TokenValidationException {

    public TokenExpiredException(Throwable cause) {
        super("Token has expired:", cause);
    }
}
