package org.webeid.security.exceptions;

/**
 * Thrown when authentication token parsing fails.
 */
public class TokenParseException extends TokenValidationException {
    public TokenParseException(Throwable cause) {
        super("Error parsing token:", cause);
    }

    public TokenParseException(String message) {
        super(message);
    }
}
