package org.webeid.security.exceptions;

/**
 * Thrown when authentication token signature validation fails.
 */
public class TokenSignatureValidationException extends TokenValidationException {
    public TokenSignatureValidationException(Throwable cause) {
        super("Token signature validation has failed:", cause);
    }
}
