package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate valid until date is in the past.
 */
public class UserCertificateExpiredException extends TokenValidationException {
    public UserCertificateExpiredException(Throwable cause) {
        super("User certificate has expired:", cause);
    }
}
