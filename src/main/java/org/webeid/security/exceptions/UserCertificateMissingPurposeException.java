package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate purpose field is missing or empty.
 */
public class UserCertificateMissingPurposeException extends TokenValidationException {
    public UserCertificateMissingPurposeException() {
        super("User certificate purpose is missing");
    }
}
