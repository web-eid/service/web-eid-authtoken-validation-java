package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate is not trusted.
 */
public class UserCertificateNotTrustedException extends TokenValidationException {
    public UserCertificateNotTrustedException() {
        super("User certificate is not trusted");
    }

    public UserCertificateNotTrustedException(String msg) {
        super("User certificate is not trusted: " + msg);
    }
}
