package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate valid from date is in the future.
 */
public class UserCertificateNotYetValidException extends TokenValidationException {
    public UserCertificateNotYetValidException(Throwable cause) {
        super("User certificate is not yet valid:", cause);
    }
}
