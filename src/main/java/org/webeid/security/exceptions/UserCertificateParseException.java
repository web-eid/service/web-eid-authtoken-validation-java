package org.webeid.security.exceptions;

/**
 * Thrown when user certificate parsing fails.
 */
public class UserCertificateParseException extends TokenValidationException {
    public UserCertificateParseException(Throwable cause) {
        super("Error parsing certificate:", cause);
    }
}
