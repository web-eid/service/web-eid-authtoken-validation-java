package org.webeid.security.exceptions;

/**
 * Thrown when user certificate revocation check with OCSP fails.
 */
public class UserCertificateRevocationCheckFailedException extends TokenValidationException {
    public UserCertificateRevocationCheckFailedException(Throwable cause) {
        super("User certificate revocation check has failed:", cause);
    }

    public UserCertificateRevocationCheckFailedException(String message) {
        super("User certificate revocation check has failed: " + message);
    }
}
