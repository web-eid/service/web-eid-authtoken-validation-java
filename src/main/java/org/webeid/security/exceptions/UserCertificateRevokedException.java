package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate has been revoked.
 */
public class UserCertificateRevokedException extends TokenValidationException {
    public UserCertificateRevokedException() {
        super("User certificate has been revoked");
    }

    public UserCertificateRevokedException(String msg) {
        super("User certificate has been revoked: " + msg);
    }
}
