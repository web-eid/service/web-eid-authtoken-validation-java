package org.webeid.security.exceptions;

/**
 * Thrown when the user certificate purpose is not client authentication.
 */
public class UserCertificateWrongPurposeException extends TokenValidationException {
    public UserCertificateWrongPurposeException() {
        super("User certificate is not meant to be used as an authentication certificate");
    }
}
