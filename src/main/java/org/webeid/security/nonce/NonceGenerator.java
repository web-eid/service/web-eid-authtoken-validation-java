package org.webeid.security.nonce;

/**
 * Generates cryptographic nonces.
 */
public interface NonceGenerator {

    int NONCE_LENGTH = 32;

    /**
     * Generates a cryptographic nonce, a large random number that can be used only once,
     * and stores it in cache.
     *
     * @return Base64-encoded nonce
     */
    String generateAndStoreNonce();
}
