package org.webeid.security.nonce;

import javax.cache.Cache;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Base64;

final class NonceGeneratorImpl implements NonceGenerator {

    private final Cache<String, LocalDateTime> cache;
    private final SecureRandom secureRandom;
    private final Duration ttl;

    NonceGeneratorImpl(Cache<String, LocalDateTime> cache, SecureRandom secureRandom, Duration ttl) {
        this.cache = cache;
        this.secureRandom = secureRandom;
        this.ttl = ttl;
    }

    @Override
    public String generateAndStoreNonce() {
        final byte[] nonceBytes = new byte[NONCE_LENGTH];
        secureRandom.nextBytes(nonceBytes);
        final LocalDateTime expirationTime = LocalDateTime.now().plus(ttl);
        final String base64StringNonce = Base64.getEncoder().encodeToString(nonceBytes);
        cache.put(base64StringNonce, expirationTime);
        return base64StringNonce;
    }
}
