package org.webeid.security.validator;

import java.security.cert.X509Certificate;

/**
 * Context data class used internally by validators.
 * <p>
 * Not part of public API.
 */
public final class AuthTokenValidatorData {

    private final X509Certificate subjectCertificate;
    private String nonce;
    private String origin;
    private String siteCertificateFingerprint;

    AuthTokenValidatorData(X509Certificate subjectCertificate) {
        this.subjectCertificate = subjectCertificate;
    }

    public X509Certificate getSubjectCertificate() {
        return subjectCertificate;
    }

    public String getNonce() {
        return nonce;
    }

    void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getOrigin() {
        return origin;
    }

    void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSiteCertificateFingerprint() {
        return siteCertificateFingerprint;
    }

    public void setSiteCertificateFingerprint(String siteCertificateFingerprint) {
        this.siteCertificateFingerprint = siteCertificateFingerprint;
    }
}
