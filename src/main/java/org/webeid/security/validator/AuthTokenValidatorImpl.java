package org.webeid.security.validator;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.validator.validators.*;

import java.security.cert.X509Certificate;

/**
 * Provides default implementation of {@link AuthTokenValidator}.
 */
final class AuthTokenValidatorImpl implements AuthTokenValidator {

    private static final int TOKEN_MIN_LENGTH = 100;
    private static final int TOKEN_MAX_LENGTH = 10000;
    private static final Logger LOG = LoggerFactory.getLogger(AuthTokenValidatorImpl.class);

    private final AuthTokenValidationConfiguration configuration;
    /*
     * OkHttp performs best when a single OkHttpClient instance is created and reused for all HTTP calls.
     * This is because each client holds its own connection pool and thread pools.
     * Reusing connections and threads reduces latency and saves memory.
     */
    private final Supplier<OkHttpClient> httpClientSupplier;
    private final ValidatorBatch simpleSubjectCertificateValidators;
    private final ValidatorBatch tokenBodyValidators;

    /**
     * @param configuration configuration parameters for the token validator
     */
    AuthTokenValidatorImpl(AuthTokenValidationConfiguration configuration) {
        // Clone the configuration object to make AuthTokenValidatorImpl immutable and thread-safe.
        this.configuration = configuration.clone();
        /*
         * Lazy initialization, avoid constructing the OkHttpClient object when certificate revocation check is not enabled.
         * Returns a supplier which caches the instance retrieved during the first call to get() and returns
         * that value on subsequent calls to get(). The returned supplier is thread-safe.
         * The OkHttpClient build() method will be invoked at most once.
         */
        this.httpClientSupplier = Suppliers.memoize(() -> new OkHttpClient.Builder()
            .connectTimeout(configuration.getOcspRequestTimeout())
            .callTimeout(configuration.getOcspRequestTimeout())
            .build()
        );

        simpleSubjectCertificateValidators = ValidatorBatch.createFrom(
            FunctionalSubjectCertificateValidators::validateCertificateExpiry,
            FunctionalSubjectCertificateValidators::validateCertificatePurpose
        );
        tokenBodyValidators = ValidatorBatch.createFrom(
            new NonceValidator(configuration.getNonceCache())::validateNonce,
            new OriginValidator(configuration.getSiteOrigin())::validateOrigin
        ).addOptional(configuration.isSiteCertificateFingerprintValidationEnabled(),
            new SiteCertificateFingerprintValidator(configuration.getSiteCertificateSha256Fingerprint())::validateSiteCertificateFingerprint
        );
    }

    @Override
    public X509Certificate validate(String authToken) throws TokenValidationException {

        if (authToken == null || authToken.length() < TOKEN_MIN_LENGTH) {
            throw new TokenParseException("Auth token is null or too short");
        }
        if (authToken.length() > TOKEN_MAX_LENGTH) {
            throw new TokenParseException("Auth token is too long");
        }

        try {
            LOG.info("Starting token parsing and validation");
            final AuthTokenParser authTokenParser = new AuthTokenParser(authToken, configuration.getAllowedClientClockSkew());
            final AuthTokenValidatorData actualTokenData = authTokenParser.parseHeaderFromTokenString();

            simpleSubjectCertificateValidators.executeFor(actualTokenData);

            final SubjectCertificateTrustedValidator certTrustedValidator =
                new SubjectCertificateTrustedValidator(configuration.getTrustedCACertificates());
            final ValidatorBatch certTrustValidators = ValidatorBatch.createFrom(
                certTrustedValidator::validateCertificateTrusted
            ).addOptional(configuration.isUserCertificateRevocationCheckWithOcspEnabled(),
                new SubjectCertificateNotRevokedValidator(certTrustedValidator, httpClientSupplier.get())::validateCertificateNotRevoked
            );
            certTrustValidators.executeFor(actualTokenData);

            authTokenParser.validateTokenSignatureAndParseClaims();
            authTokenParser.populateDataFromClaims(actualTokenData);

            tokenBodyValidators.executeFor(actualTokenData);

            LOG.info("Token parsing and validation successful");
            return actualTokenData.getSubjectCertificate();

        } catch (Exception e) {
            LOG.warn("Token parsing and validation was interrupted:", e);
            throw e;
        }
    }

}
