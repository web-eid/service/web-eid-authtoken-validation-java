package org.webeid.security.validator.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.*;
import org.webeid.security.validator.AuthTokenValidatorData;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.util.List;

public final class FunctionalSubjectCertificateValidators {

    private static final Logger LOG = LoggerFactory.getLogger(FunctionalSubjectCertificateValidators.class);
    private static final String EXTENDED_KEY_USAGE_CLIENT_AUTHENTICATION = "1.3.6.1.5.5.7.3.2";

    /**
     * Checks the validity of the user certificate from the authentication token.
     *
     * @param actualTokenData authentication token data that contains the user certificate
     * @throws TokenValidationException when the user certificate is expired or not yet valid
     */
    public static void validateCertificateExpiry(AuthTokenValidatorData actualTokenData) throws TokenValidationException {
        try {
            actualTokenData.getSubjectCertificate().checkValidity();
            LOG.debug("User certificate is valid.");
        } catch (CertificateNotYetValidException e) {
            throw new UserCertificateNotYetValidException(e);
        } catch (CertificateExpiredException e) {
            throw new UserCertificateExpiredException(e);
        }
    }

    /**
     * Validates that the purpose of the user certificate from the authentication token contains client authentication.
     *
     * @param actualTokenData authentication token data that contains the user certificate
     * @throws TokenValidationException when the purpose of certificate does not contain client authentication
     */
    public static void validateCertificatePurpose(AuthTokenValidatorData actualTokenData) throws TokenValidationException {
        try {
            final List<String> usages = actualTokenData.getSubjectCertificate().getExtendedKeyUsage();
            if (usages == null || usages.isEmpty()) {
                throw new UserCertificateMissingPurposeException();
            }
            if (!usages.contains(EXTENDED_KEY_USAGE_CLIENT_AUTHENTICATION)) {
                throw new UserCertificateWrongPurposeException();
            }
            LOG.debug("User certificate can be used for client authentication.");
        } catch (CertificateParsingException e) {
            throw new UserCertificateParseException(e);
        }
    }
}
