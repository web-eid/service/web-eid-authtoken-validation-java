package org.webeid.security.validator.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.NonceExpiredException;
import org.webeid.security.exceptions.NonceNotFoundException;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.nonce.NonceGenerator;
import org.webeid.security.validator.AuthTokenValidatorData;

import javax.cache.Cache;
import java.time.LocalDateTime;

public final class NonceValidator {

    private static final Logger LOG = LoggerFactory.getLogger(NonceValidator.class);

    private final Cache<String, LocalDateTime> cache;

    public NonceValidator(Cache<String, LocalDateTime> cache) {
        this.cache = cache;
    }

    /**
     * Validates that the nonce from the authentication token has the correct
     * length, exists in cache and has not expired.
     *
     * @param actualTokenData authentication token data that contains the nonce
     * @throws TokenValidationException when the nonce in the token does not
     *                                  meet the requirements
     */
    public void validateNonce(AuthTokenValidatorData actualTokenData) throws TokenValidationException {
        final String nonce = actualTokenData.getNonce();
        final LocalDateTime nonceExpirationTime = cache.getAndRemove(nonce);
        if (nonceExpirationTime == null) {
            throw new NonceNotFoundException();
        }
        if (nonce.length() < NonceGenerator.NONCE_LENGTH) {
            throw new TokenParseException(String.format(
                "Nonce is shorter than the required length of %d.",
                NonceGenerator.NONCE_LENGTH
            ));
        }
        if (nonceExpirationTime.isBefore(LocalDateTime.now())) {
            throw new NonceExpiredException();
        }
        LOG.debug("Nonce was found and has not expired.");
    }
}
