package org.webeid.security.validator.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.SiteCertificateFingerprintValidationException;
import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.validator.AuthTokenValidatorData;

public final class SiteCertificateFingerprintValidator {

    private static final Logger LOG = LoggerFactory.getLogger(SiteCertificateFingerprintValidator.class);

    private final String expectedSiteCertificateFingerprint;

    public SiteCertificateFingerprintValidator(String expectedSiteCertificateFingerprint) {
        this.expectedSiteCertificateFingerprint = expectedSiteCertificateFingerprint;
    }

    /**
     * Validates that the site certificate fingerprint from the authentication token matches with
     * the configured site certificate fingerprint.
     *
     * @param actualTokenData authentication token data that contains the site fingerprint from authentication token
     * @throws TokenValidationException when fingerprints don't match
     */
    public void validateSiteCertificateFingerprint(AuthTokenValidatorData actualTokenData) throws TokenValidationException {
        if (!expectedSiteCertificateFingerprint.equals(actualTokenData.getSiteCertificateFingerprint())) {
            throw new SiteCertificateFingerprintValidationException();
        }
        LOG.debug("Site certificate fingerprint is equal to expected fingerprint.");
    }
}
