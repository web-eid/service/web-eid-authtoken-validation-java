package org.webeid.security.validator.validators;

import okhttp3.OkHttpClient;
import org.bouncycastle.asn1.ocsp.OCSPResponseStatus;
import org.bouncycastle.cert.ocsp.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.exceptions.UserCertificateRevocationCheckFailedException;
import org.webeid.security.exceptions.UserCertificateRevokedException;
import org.webeid.security.validator.AuthTokenValidatorData;
import org.webeid.security.validator.ocsp.OcspRequestBuilder;
import org.webeid.security.validator.ocsp.OcspUtils;

import java.io.IOException;
import java.net.URI;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Objects;

public final class SubjectCertificateNotRevokedValidator {

    private static final Logger LOG = LoggerFactory.getLogger(SubjectCertificateNotRevokedValidator.class);

    private final SubjectCertificateTrustedValidator trustValidator;
    private final OkHttpClient httpClient;

    public SubjectCertificateNotRevokedValidator(SubjectCertificateTrustedValidator trustValidator, OkHttpClient httpClient) {
        this.trustValidator = trustValidator;
        this.httpClient = httpClient;
    }

    /**
     * Validates that the user certificate from the authentication token is not revoked with OCSP.
     *
     * @param actualTokenData authentication token data that contains the user certificate.
     * @throws TokenValidationException when user certificate is revoked.
     */
    public void validateCertificateNotRevoked(AuthTokenValidatorData actualTokenData) throws TokenValidationException {
        try {
            final X509Certificate certificate = actualTokenData.getSubjectCertificate();
            final URI uri = OcspUtils.ocspUri(certificate);

            if (uri == null) {
                throw new UserCertificateRevocationCheckFailedException("The CA/certificate doesn't have an OCSP responder");
            }

            final OCSPReq request = new OcspRequestBuilder()
                .certificate(certificate)
                .issuer(Objects.requireNonNull(trustValidator.getSubjectCertificateIssuerCertificate()))
                .build();

            LOG.debug("Sending OCSP request");
            final OCSPResp response = OcspUtils.request(uri, request, httpClient);
            if (response.getStatus() != OCSPResponseStatus.SUCCESSFUL) {
                throw new UserCertificateRevocationCheckFailedException("Response status: " + response.getStatus());
            }

            final BasicOCSPResp basicResponse = (BasicOCSPResp) response.getResponseObject();
            final SingleResp first = basicResponse.getResponses()[0];

            final CertificateStatus status = first.getCertStatus();

            if (status instanceof RevokedStatus) {
                RevokedStatus revokedStatus = (RevokedStatus) status;
                throw (revokedStatus.hasRevocationReason() ?
                    new UserCertificateRevokedException("Revocation reason: " + revokedStatus.getRevocationReason()) :
                    new UserCertificateRevokedException());
            } else if (status instanceof UnknownStatus) {
                throw new UserCertificateRevokedException("Unknown status");
            } else if (status == null) {
                LOG.debug("OCSP check result is GOOD");
            } else {
                throw new UserCertificateRevokedException("Status is neither good, revoked nor unknown");
            }
        } catch (CertificateEncodingException | OCSPException | IOException e) {
            throw new UserCertificateRevocationCheckFailedException(e);
        }
    }
}
