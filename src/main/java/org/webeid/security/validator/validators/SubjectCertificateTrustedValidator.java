package org.webeid.security.validator.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webeid.security.exceptions.UserCertificateNotTrustedException;
import org.webeid.security.validator.AuthTokenValidatorData;

import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Collection;

public final class SubjectCertificateTrustedValidator {

    private static final Logger LOG = LoggerFactory.getLogger(SubjectCertificateTrustedValidator.class);

    private final Collection<X509Certificate> trustedCACertificates;
    private X509Certificate trustedCACertificate;

    public SubjectCertificateTrustedValidator(Collection<X509Certificate> trustedCACertificates) {
        this.trustedCACertificates = trustedCACertificates;
    }

    /**
     * Validates that the user certificate from the authentication token is signed by a trusted certificate authority.
     *
     * @param actualTokenData authentication token data that contains the user certificate.
     * @throws UserCertificateNotTrustedException when user certificate is not signed by a trusted CA or is valid after CA certificate.
     */
    public void validateCertificateTrusted(AuthTokenValidatorData actualTokenData) throws UserCertificateNotTrustedException {

        final X509Certificate certificate = actualTokenData.getSubjectCertificate();

        for (final X509Certificate trustedCACertificate : trustedCACertificates) {
            try {
                certificate.verify(trustedCACertificate.getPublicKey());
                if (certificate.getNotAfter().after(trustedCACertificate.getNotAfter())) {
                    throw new UserCertificateNotTrustedException("Trusted CA certificate expires earlier than the user certificate");
                }
                this.trustedCACertificate = trustedCACertificate;
                LOG.debug("User certificate is signed with a trusted CA certificate");
                return;
            } catch (GeneralSecurityException e) {
                LOG.trace("Error verifying signer's certificate {} against CA certificate {}", certificate.getSubjectDN(), trustedCACertificate.getSubjectDN());
            }
        }
        throw new UserCertificateNotTrustedException();
    }

    public X509Certificate getSubjectCertificateIssuerCertificate() {
        return trustedCACertificate;
    }
}
