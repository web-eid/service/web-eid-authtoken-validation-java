package org.webeid.security.validator.validators;

import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.validator.AuthTokenValidatorData;

/**
 * Validators perform the actual validation actions.
 * <p>
 * They are used by AuthTokenValidatorImpl and are not part of the public API.
 */
@FunctionalInterface
public interface Validator {
    void validate(AuthTokenValidatorData data) throws TokenValidationException;
}
