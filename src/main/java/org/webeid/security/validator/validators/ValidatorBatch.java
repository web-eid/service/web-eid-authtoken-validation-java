package org.webeid.security.validator.validators;

import com.google.common.collect.Lists;
import org.webeid.security.exceptions.TokenValidationException;
import org.webeid.security.validator.AuthTokenValidatorData;

import java.util.List;

public final class ValidatorBatch {

    private final List<Validator> validatorList;

    public static ValidatorBatch createFrom(Validator... validatorList) {
        return new ValidatorBatch(Lists.newArrayList(validatorList));
    }

    public void executeFor(AuthTokenValidatorData data) throws TokenValidationException {
        for (final Validator validator : validatorList) {
            validator.validate(data);
        }
    }

    public ValidatorBatch addOptional(boolean condition, Validator optionalValidator) {
        if (condition) {
            validatorList.add(optionalValidator);
        }
        return this;
    }

    private ValidatorBatch(List<Validator> validatorList) {
        this.validatorList = validatorList;
    }
}
