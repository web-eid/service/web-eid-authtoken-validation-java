package org.webeid.security.nonce;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.webeid.security.testutil.AbstractTestWithCache;

import static org.assertj.core.api.Assertions.assertThat;

class NonceGeneratorTest extends AbstractTestWithCache {
    private NonceGenerator nonceGenerator;

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        nonceGenerator = new NonceGeneratorBuilder()
            .withNonceCache(cache)
            .build();
    }

    @Test
    public void validateNonceGeneration() {
        final String nonce1 = nonceGenerator.generateAndStoreNonce();
        final String nonce2 = nonceGenerator.generateAndStoreNonce();

        assertThat(nonce1.length()).isEqualTo(44); // Base64-encoded 32 bytes
        assertThat(nonce1).isNotEqualTo(nonce2);
        // It might be possible to add an entropy test by compressing the nonce bytes
        // and verifying that the result is longer than for non-random strings.
    }

}
