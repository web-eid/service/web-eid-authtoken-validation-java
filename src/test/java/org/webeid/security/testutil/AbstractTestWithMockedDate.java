package org.webeid.security.testutil;

import org.junit.jupiter.api.BeforeEach;

import java.text.ParseException;

public abstract class AbstractTestWithMockedDate extends AbstractTestWithCache {

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        try {
            // Authentication token is valid until 2020-04-14
            Dates.setMockedDate(Dates.create("2020-04-11"));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
