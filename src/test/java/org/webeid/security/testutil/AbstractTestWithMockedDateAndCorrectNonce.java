package org.webeid.security.testutil;

import org.junit.jupiter.api.BeforeEach;

public abstract class AbstractTestWithMockedDateAndCorrectNonce extends AbstractTestWithMockedDate {

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        putCorrectNonceToCache();
    }

}
