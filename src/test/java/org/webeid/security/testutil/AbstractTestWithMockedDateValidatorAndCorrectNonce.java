package org.webeid.security.testutil;

import org.junit.jupiter.api.BeforeEach;
import org.webeid.security.validator.AuthTokenValidator;

import java.security.cert.CertificateException;

import static org.webeid.security.testutil.AuthTokenValidators.getAuthTokenValidator;

public abstract class AbstractTestWithMockedDateValidatorAndCorrectNonce extends AbstractTestWithMockedDateAndCorrectNonce {

    protected AuthTokenValidator validator;

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        try {
            validator = getAuthTokenValidator(cache);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
    }

}
