package org.webeid.security.testutil;

import org.junit.jupiter.api.BeforeEach;

public abstract class AbstractTestWithValidatorAndCorrectNonce extends AbstractTestWithValidator {

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        putCorrectNonceToCache();
    }

}
