package org.webeid.security.testutil;

import org.webeid.security.validator.AuthTokenValidator;
import org.webeid.security.validator.AuthTokenValidatorBuilder;

import javax.cache.Cache;
import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;

public final class AuthTokenValidators {

    private static final String TOKEN_ORIGIN_URL = "https://ria.ee";

    public static AuthTokenValidator getAuthTokenValidator(Cache<String, LocalDateTime> cache) throws CertificateException {
        return getAuthTokenValidator(TOKEN_ORIGIN_URL, cache);
    }

    public static AuthTokenValidator getAuthTokenValidator(String url, Cache<String, LocalDateTime> cache) throws CertificateException {
        return getAuthTokenValidator(url, cache, getCACertificates());
    }

    public static AuthTokenValidator getAuthTokenValidator(String url, Cache<String, LocalDateTime> cache, X509Certificate... certificates) {
        return getAuthTokenValidatorBuilder(url, cache, certificates)
            .withoutUserCertificateRevocationCheckWithOcsp()
            .build();
    }

    public static AuthTokenValidator getAuthTokenValidator(Cache<String, LocalDateTime> cache, String certFingerprint) throws CertificateException {
        return getAuthTokenValidatorBuilder(TOKEN_ORIGIN_URL, cache, getCACertificates())
            .withSiteCertificateSha256Fingerprint(certFingerprint)
            .withoutUserCertificateRevocationCheckWithOcsp()
            .build();
    }

    public static AuthTokenValidator getAuthTokenValidatorWithOcspCheck(Cache<String, LocalDateTime> cache) throws CertificateException {
        return getAuthTokenValidatorBuilder(TOKEN_ORIGIN_URL, cache, getCACertificates()).build();
    }

    public static AuthTokenValidator getAuthTokenValidatorWithWrongTrustedCA(Cache<String, LocalDateTime> cache) throws CertificateException {
        return getAuthTokenValidator(TOKEN_ORIGIN_URL, cache,
            CertificateLoader.loadCertificatesFromResources("ESTEID2018.cer"));
    }

    private static AuthTokenValidatorBuilder getAuthTokenValidatorBuilder(String uri, Cache<String, LocalDateTime> cache, X509Certificate[] certificates) {
        return new AuthTokenValidatorBuilder()
            .withSiteOrigin(URI.create(uri))
            .withNonceCache(cache)
            .withTrustedCertificateAuthorities(certificates);
    }

    private static X509Certificate[] getCACertificates() throws CertificateException {
        return CertificateLoader.loadCertificatesFromResources("TEST_of_ESTEID2018.cer");
    }
}
