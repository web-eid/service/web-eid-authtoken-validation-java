package org.webeid.security.testutil;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import mockit.Mock;
import mockit.MockUp;

import java.text.ParseException;
import java.util.Date;

public final class Dates {
    private static final StdDateFormat STD_DATE_FORMAT = new StdDateFormat();

    public static Date create(String iso8601Date) throws ParseException {
        return STD_DATE_FORMAT.parse(iso8601Date);
    }

    public static void setMockedDate(Date mockedDate) {
        new MockUp<System>() {
            @Mock
            public long currentTimeMillis() {
                return mockedDate.getTime();
            }
        };
    }

}
