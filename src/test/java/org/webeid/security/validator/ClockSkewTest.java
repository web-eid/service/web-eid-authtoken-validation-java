package org.webeid.security.validator;

import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.TokenExpiredException;
import org.webeid.security.testutil.AbstractTestWithValidatorAndCorrectNonce;
import org.webeid.security.testutil.Dates;
import org.webeid.security.testutil.Tokens;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ClockSkewTest extends AbstractTestWithValidatorAndCorrectNonce {

    @Test
    public void blockLargeClockSkew4min() throws Exception {
        // Authentication token expires at 2020-04-14 13:32:49
        Dates.setMockedDate(Dates.create("2020-04-14T13:36:49Z"));
        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(TokenExpiredException.class);
    }

    @Test
    public void allowSmallClockSkew2min() throws Exception {
        // Authentication token expires at 2020-04-14 13:32:49
        Dates.setMockedDate(Dates.create("2020-04-14T13:30:49Z"));
        assertThatCode(() -> validator.validate(Tokens.SIGNED))
            .doesNotThrowAnyException();
    }

}
