package org.webeid.security.validator;

import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.NonceExpiredException;
import org.webeid.security.exceptions.NonceNotFoundException;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.testutil.AbstractTestWithValidator;
import org.webeid.security.testutil.Dates;
import org.webeid.security.testutil.Tokens;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class NonceTest extends AbstractTestWithValidator {

    @Test
    public void validateIncorrectNonce() {
        putIncorrectNonceToCache();
        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(NonceNotFoundException.class);
    }

    @Test
    public void validateExpiredNonce() {
        putExpiredNonceToCache();
        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(NonceExpiredException.class);
    }

    @Test
    public void testNonceMissing() throws Exception {
        Dates.setMockedDate(Dates.create("2020-04-14T13:00:00Z"));
        assertThatThrownBy(() -> validator.validate(Tokens.NONCE_MISSING))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("nonce field must be present and not empty in authentication token body");
    }

    @Test
    public void testNonceEmpty() throws Exception {
        Dates.setMockedDate(Dates.create("2020-04-14T13:00:00Z"));
        assertThatThrownBy(() -> validator.validate(Tokens.NONCE_EMPTY))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("nonce field must be present and not empty in authentication token body");
    }

    @Test
    public void testTokenNonceNotString() throws Exception {
        Dates.setMockedDate(Dates.create("2020-04-14T13:00:00Z"));
        assertThatThrownBy(() -> validator.validate(Tokens.NONCE_NOT_STRING))
            .isInstanceOf(TokenParseException.class);
    }

    @Test
    public void testNonceTooShort() throws Exception {
        Dates.setMockedDate(Dates.create("2020-04-14T13:00:00Z"));
        putTooShortNonceToCache();
        assertThatThrownBy(() -> validator.validate(Tokens.NONCE_TOO_SHORT))
            .isInstanceOf(TokenParseException.class);
    }

}
