package org.webeid.security.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.UserCertificateRevocationCheckFailedException;
import org.webeid.security.exceptions.UserCertificateRevokedException;
import org.webeid.security.testutil.AbstractTestWithMockedDateAndCorrectNonce;
import org.webeid.security.testutil.Tokens;

import java.security.cert.CertificateException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.webeid.security.testutil.AuthTokenValidators.getAuthTokenValidatorWithOcspCheck;

public class OcspTest extends AbstractTestWithMockedDateAndCorrectNonce {

    private AuthTokenValidator validator;

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        try {
            validator = getAuthTokenValidatorWithOcspCheck(cache);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void detectRevokedUserCertificate() {
        // This test used to have flaky results, due to occasionally failing
        // OCSP requests. Therefore, the failed revocation check is now handled
        // as passing.
        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOfAny(
                UserCertificateRevokedException.class,
                UserCertificateRevocationCheckFailedException.class
            );
    }

}
