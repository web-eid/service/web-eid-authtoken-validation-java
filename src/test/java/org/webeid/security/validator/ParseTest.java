package org.webeid.security.validator;

import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.exceptions.TokenSignatureValidationException;
import org.webeid.security.testutil.AbstractTestWithMockedDateValidatorAndCorrectNonce;
import org.webeid.security.testutil.Tokens;
import org.webeid.security.util.CertUtil;

import java.security.cert.X509Certificate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.webeid.security.testutil.Tokens.getUnsignedTokenString;
import static org.webeid.security.util.TitleCase.toTitleCase;

public class ParseTest extends AbstractTestWithMockedDateValidatorAndCorrectNonce {

    /*
      Validates the happy path:
      - valid user certificate with trusted issuer signature
      - token signature is correct
      - correct origin
      - nonce exists in the cache and has not expired
     */
    @Test
    public void parseSignedToken() throws Exception {
        final X509Certificate result = validator.validate(Tokens.SIGNED);
        assertThat(CertUtil.getSubjectCN(result))
            .isEqualTo("JÕEORG\\,JAAK-KRISTJAN\\,38001085718");
        assertThat(toTitleCase(CertUtil.getSubjectGivenName(result)))
            .isEqualTo("Jaak-Kristjan");
        assertThat(toTitleCase(CertUtil.getSubjectSurname(result)))
            .isEqualTo("Jõeorg");
        assertThat(CertUtil.getSubjectIdCode(result))
            .isEqualTo("PNOEE-38001085718");
        assertThat(CertUtil.getSubjectCountryCode(result))
            .isEqualTo("EE");
    }

    @Test
    public void detectUnsignedToken() {
        assertThatThrownBy(() -> validator.validate(getUnsignedTokenString()))
            .isInstanceOf(TokenSignatureValidationException.class);
    }

    @Test
    public void detectCorruptedToken() {
        assertThatThrownBy(() -> validator.validate(Tokens.CORRUPTED))
            .isInstanceOf(TokenParseException.class);
    }
}
