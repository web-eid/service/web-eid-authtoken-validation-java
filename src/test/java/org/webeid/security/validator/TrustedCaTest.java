package org.webeid.security.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.UserCertificateNotTrustedException;
import org.webeid.security.testutil.AbstractTestWithMockedDateAndCorrectNonce;
import org.webeid.security.testutil.Tokens;

import java.security.cert.CertificateException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.webeid.security.testutil.AuthTokenValidators.getAuthTokenValidatorWithWrongTrustedCA;

public class TrustedCaTest extends AbstractTestWithMockedDateAndCorrectNonce {

    private AuthTokenValidator validator;

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        try {
            validator = getAuthTokenValidatorWithWrongTrustedCA(cache);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void detectUntrustedUserCertificate() {
        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(UserCertificateNotTrustedException.class);
    }

}
