package org.webeid.security.validator;


import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.exceptions.UserCertificateExpiredException;
import org.webeid.security.exceptions.UserCertificateNotYetValidException;
import org.webeid.security.testutil.AbstractTestWithMockedDateValidatorAndCorrectNonce;
import org.webeid.security.testutil.Dates;
import org.webeid.security.testutil.Tokens;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ValidateTest extends AbstractTestWithMockedDateValidatorAndCorrectNonce {

    @Test
    public void certificateIsNotValidYet() throws Exception {
        final Date certValidFrom = Dates.create("2018-10-17");
        Dates.setMockedDate(certValidFrom);

        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(UserCertificateNotYetValidException.class);
    }

    @Test
    public void certificateIsNoLongerValid() throws Exception {
        final Date certValidFrom = Dates.create("2023-10-19");
        Dates.setMockedDate(certValidFrom);

        assertThatThrownBy(() -> validator.validate(Tokens.SIGNED))
            .isInstanceOf(UserCertificateExpiredException.class);
    }

    @Test
    public void testTokenTooShort() {
        assertThatThrownBy(() -> validator.validate(Tokens.TOKEN_TOO_SHORT))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("Auth token is null or too short");
    }

    @Test
    public void testTokenTooLong() {
        assertThatThrownBy(() -> validator.validate(Tokens.TOKEN_TOO_LONG))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("Auth token is too long");
    }

}
