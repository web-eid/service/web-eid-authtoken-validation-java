package org.webeid.security.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.webeid.security.exceptions.TokenParseException;
import org.webeid.security.exceptions.UserCertificateMissingPurposeException;
import org.webeid.security.exceptions.UserCertificateWrongPurposeException;
import org.webeid.security.testutil.AbstractTestWithValidatorAndCorrectNonce;
import org.webeid.security.testutil.Dates;
import org.webeid.security.testutil.Tokens;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class X5cTest extends AbstractTestWithValidatorAndCorrectNonce {

    @Override
    @BeforeEach
    public void setup() {
        super.setup();
        try {
            // Ensure that certificate is valid
            Dates.setMockedDate(Dates.create("2020-09-25"));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testX5cMissing() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_MISSING))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("x5c field must be present");
    }

    @Test
    public void testX5cNotArray() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_NOT_ARRAY))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("x5c field in authentication token header must be an array");
    }

    @Test
    public void testX5cEmpty() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_EMPTY))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("Certificate from x5c field must not be empty");
    }

    @Test
    public void testX5cNotString() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_NOT_STRING))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("x5c field in authentication token header must be an array of strings");
    }

    @Test
    public void testX5cInvalidCertificate() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_INVALID_CERTIFICATE))
            .isInstanceOf(TokenParseException.class)
            .hasMessageStartingWith("x5c field must contain a valid certificate");
    }

    @Test
    public void testX5cMissingPurposeCertificate() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_MISSING_PURPOSE_CERTIFICATE))
            .isInstanceOf(UserCertificateMissingPurposeException.class);
    }

    @Test
    public void testX5cWrongPurposeCertificate() {
        assertThatThrownBy(() -> validator.validate(Tokens.X5C_WRONG_PURPOSE_CERTIFICATE))
            .isInstanceOf(UserCertificateWrongPurposeException.class);
    }

}
